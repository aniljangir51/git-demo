package com.anil.gitdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anil.gitdemo.adapters.IssuesAdpter
import com.anil.gitdemo.databinding.ActivityMainBinding
import com.anil.gitdemo.interfaces.ItemClickListener
import com.anil.gitdemo.network.model.Issues
import com.anil.gitdemo.network.model.Response
import com.anil.gitdemo.network.model.Status
import com.anil.gitdemo.utils.MyDividerItemDecoration
import com.anil.gitdemo.viewmodels.MainViewModel
import io.reactivex.observers.DisposableSingleObserver

class MainActivity : AppCompatActivity(), ItemClickListener {

    var mList=ArrayList<Issues>()
    var adapter = IssuesAdpter(this,mList,this)
    lateinit var binding: ActivityMainBinding
    lateinit var mModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        initAdapter()


        mModel= ViewModelProviders.of(this).get(MainViewModel::class.java)

        mModel.responce.observe(this, Observer {
            processResponce(it)
        })
        mModel.getIssues()

    }

    private fun processResponce(it: Response?) {
        when(it?.status){
            Status.SUCCESS ->{
                binding.isLoading=false
                mList.clear()
                mList.addAll(it.data)
                adapter.notifyDataSetChanged()
            }

            Status.ERROR ->{
                binding.isLoading=false
                binding.isNodata=true
            }
            Status.LOADING ->{
                binding.isLoading=true
            }
        }

    }

    private fun initAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        binding.recyclerView.itemAnimator = DefaultItemAnimator()
        //  binding.recyclerView.addItemDecoration(MyDividerItemDecoration(this,RecyclerView.VERTICAL,16))
        binding.recyclerView.adapter = adapter
    }

    override fun onItemClick(v: View?, pos: Int) {
        Log.e("clicked",""+pos)
        startActivity(Intent(this,DetailActivity::class.java)
            .putExtra("model",mList.get(pos)))
    }
}
