package com.anil.gitdemo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

import com.anil.gitdemo.application.MyUIApplication;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.RequestOptions;

import java.security.MessageDigest;

public class ImageUtlis {
    @BindingAdapter(value = {"imageurl","placeholder","imgwidth","imgheight"},requireAll = false)
    public static void imageurl(ImageView view, String url, Drawable placeholder,int imgwidth,int imgheight){
        RequestBuilder manager=Glide.with(MyUIApplication.getContext()).load(url).transform(new CircleTransform());
//        if (error==null) {
//            manager.error(R.mipmap.ic_launcher);// On error image
//
//            manager.placeholder(R.mipmap.ic_launcher);// Place holder image
//        }else {
            manager.error(placeholder);// On error image

            manager.placeholder(placeholder);// Place holder image
       // }

        if (imgheight>0 && imgwidth>0)
            manager.apply(new RequestOptions().override(imgwidth,imgheight));

        manager.into(view);
    }




public static class CircleTransform extends BitmapTransformation {


    @Override protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return circleCrop(pool, toTransform);
    }

    private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
        if (source == null) return null;

        int size = Math.min(source.getWidth(), source.getHeight());
        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        // TODO this could be acquired from the pool too
        Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

        Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
        if (result == null) {
            result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
        paint.setAntiAlias(true);
        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);
        return result;
    }



    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

    }
}
}
