package com.anil.gitdemo.utils;

import android.util.Log;

import androidx.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {
    static DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
    @TypeConverter
    public static Date toDate(String value) {
        if (value != null) {
            try {
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }
    @TypeConverter
    public static String fromDate(Date date) {

        if (date != null) {

            Log.e("mdate",date.toString());
            try {
               // return df.format(date);
                return DateFormat.getDateInstance(DateFormat.SHORT).format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

   /* @TypeConverter
    public static Date toDate(Long dateLong){

        return dateLong == null ? null: new Date(dateLong);
    }

    @TypeConverter
    public static Long fromDate(Date date){
        return date == null ? null : date.getTime();
    }*/
}