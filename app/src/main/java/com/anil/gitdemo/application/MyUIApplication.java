package com.anil.gitdemo.application;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.anil.gitdemo.database.AppDatabase;
import com.anil.gitdemo.database.DatabaseClient;

public class MyUIApplication extends Application {


    public static final String TAG = MyUIApplication.class.getSimpleName();
    private static MyUIApplication instance;
    private AppDatabase appDatabase;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;


        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "GitDemo")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries().build();
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

    public static synchronized MyUIApplication getInstance() {
        return instance;
    }









}
