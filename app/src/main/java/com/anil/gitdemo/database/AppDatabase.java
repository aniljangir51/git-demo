package com.anil.gitdemo.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.anil.gitdemo.network.model.DateModel;
import com.anil.gitdemo.network.model.Issues;
import com.anil.gitdemo.utils.DateConverter;

@Database(entities = {Issues.class, DateModel.class}, version = 3,exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract IssuesDataDeo issueDataDeo();
    public abstract DatesDataDeo dateDataDeo();

}

