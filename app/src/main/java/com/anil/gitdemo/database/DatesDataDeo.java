package com.anil.gitdemo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.anil.gitdemo.network.model.DateModel;
import com.anil.gitdemo.network.model.Issues;

import java.util.Date;
import java.util.List;

import io.reactivex.Single;

@Dao
public interface DatesDataDeo {

    @Insert
    List<Long> insert(DateModel... model);

    @Query("SELECT * FROM Date")
    List<DateModel> getAllData();

    @Query("SELECT * FROM Date where created = :date")
    List<DateModel> getDatabyDate(String date);

    @Query("DELETE FROM Date")
    int deleteAll();

}
