package com.anil.gitdemo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anil.gitdemo.network.model.Issues;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface IssuesDataDeo {

    @Insert
    List<Long> insert(Issues... issues);

    @Delete
    int delete(Issues cartData);

    @Query("SELECT * FROM Issues")
    List<Issues> getIssues();

    @Delete
    int deletedata(Issues... cartData);

    @Query("DELETE FROM Issues")
    int deleteAll();
}
