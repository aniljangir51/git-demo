package com.anil.gitdemo.database;


import com.anil.gitdemo.application.MyUIApplication;
import com.anil.gitdemo.network.model.Issues;

import java.util.List;

public class DatabaseUtlis {


    public static void InsertCartValue(List<Issues> issues){

        DatabaseClient.getInstance(MyUIApplication.getContext()).getAppDatabase()
                .issueDataDeo()
                .insert(issues.toArray(new Issues[issues.size()]));
    }

    public static void DeleteAllIssues(){
        DatabaseClient.getInstance(MyUIApplication.getContext()).getAppDatabase()
                .issueDataDeo()
                .deleteAll();
    }

}
