package com.anil.gitdemo.interfaces;

/**
 * Created by amit on 9/20/2018.
 */

import android.view.View;

public interface ItemClickListener {

    void onItemClick(View v, int pos);
}