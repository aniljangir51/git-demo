package com.anil.gitdemo.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.anil.gitdemo.R;
import com.anil.gitdemo.databinding.IssuesRowBinding;
import com.anil.gitdemo.interfaces.ItemClickListener;
import com.anil.gitdemo.network.model.Issues;

import java.util.List;

public class IssuesAdpter extends RecyclerView.Adapter<IssuesAdpter.MyViewHolder> {
    private ItemClickListener onItemClickListener;
    Context context;
    private List<Issues> mList;



    public IssuesAdpter(Context ctx, List<Issues> list,ItemClickListener onItemClickListener){
        this.context=ctx;
        this.mList = list;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public IssuesAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        IssuesRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.issues_row, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(IssuesAdpter.MyViewHolder holder, final int position) {
        Issues model=mList.get(position);

        holder.getRowBinding().setModel(model);
        holder.getRowBinding().executePendingBindings();

        holder.getRowBinding().getRoot().setOnClickListener((v) ->{
            if (onItemClickListener!=null)
                onItemClickListener.onItemClick(v,position);
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        IssuesRowBinding binding;



        public MyViewHolder(IssuesRowBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }

        public IssuesRowBinding getRowBinding() {
            return binding;
        }



    }
}
