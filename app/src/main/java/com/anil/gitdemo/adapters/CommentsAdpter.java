package com.anil.gitdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anil.gitdemo.R;
import com.anil.gitdemo.databinding.CommentsRowBinding;
import com.anil.gitdemo.databinding.IssuesRowBinding;
import com.anil.gitdemo.interfaces.ItemClickListener;
import com.anil.gitdemo.network.model.Comments;

import java.util.List;

public class CommentsAdpter extends RecyclerView.Adapter<CommentsAdpter.MyViewHolder> {
    private ItemClickListener onItemClickListener;
    Context context;
    private List<Comments> mList;



    public CommentsAdpter(Context ctx, List<Comments> list, ItemClickListener onItemClickListener){
        this.context=ctx;
        this.mList = list;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public CommentsAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CommentsRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.comments_row, parent, false);
        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(CommentsAdpter.MyViewHolder holder, final int position) {
        Comments model=mList.get(position);

        holder.getRowBinding().setModel(model);
        holder.getRowBinding().executePendingBindings();


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CommentsRowBinding binding;



        public MyViewHolder(CommentsRowBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public CommentsRowBinding getRowBinding() {
            return binding;
        }


        @Override
        public void onClick(View v) {
            if (onItemClickListener!=null)
                onItemClickListener.onItemClick(v,getAdapterPosition());
        }
    }
}
