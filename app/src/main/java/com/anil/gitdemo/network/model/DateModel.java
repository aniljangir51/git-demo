package com.anil.gitdemo.network.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.anil.gitdemo.utils.DateConverter;

import java.util.Date;
import java.util.List;

import static com.anil.gitdemo.network.model.Status.ERROR;
import static com.anil.gitdemo.network.model.Status.LOADING;
import static com.anil.gitdemo.network.model.Status.SUCCESS;

@Entity(tableName = "Date")
public class DateModel {

    @PrimaryKey(autoGenerate = true)
    public int uid;


    private String created;

    public DateModel() {

    }

    public DateModel(Date date) {
        created=DateConverter.fromDate(date);
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "DateModel{" +
                "uid=" + uid +
                ", created='" + created + '\'' +
                '}';
    }
}