package com.anil.gitdemo.network.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import static com.anil.gitdemo.network.model.Status.ERROR;
import static com.anil.gitdemo.network.model.Status.LOADING;
import static com.anil.gitdemo.network.model.Status.SUCCESS;

public class Response {

    public final Status status;


    public  List<Issues> data;

    @Nullable
    public final Throwable error;

    private Response(Status status, @Nullable List<Issues> data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static Response loading() {
        return new Response(LOADING, null, null);
    }

    public static Response success(@NonNull List<Issues> data) {
        return new Response(SUCCESS, data, null);
    }

    public static Response error(@NonNull Throwable error) {
        return new Response(ERROR, null, error);
    }
}