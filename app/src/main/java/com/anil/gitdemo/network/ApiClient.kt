@file:JvmName("ApiClient")
package com.anil.gitdemo.network

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit



var retrofit:Retrofit?=null
const val REQUEST_TIMEOUT =60
var okHttpClient:OkHttpClient?=null

fun getClient(): Retrofit?{

    if (okHttpClient == null)
        initOkHttp();


    if (retrofit == null) {
        retrofit = Retrofit.Builder()
            .baseUrl(Const.BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    return retrofit
}

fun initOkHttp() {

    val httpClient = OkHttpClient().newBuilder()
        .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

    httpClient.addInterceptor(interceptor)
    httpClient.addInterceptor(object : Interceptor{
        override fun intercept(chain: Interceptor.Chain): Response {
            val original: Request = chain.request()
            val requestBuilder: Request.Builder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Request-Type", "Android")
                .addHeader("Content-Type", "application/json")
            val request: Request = requestBuilder.build()
            return chain.proceed(request)
        }

    })

    okHttpClient = httpClient.build()
}

