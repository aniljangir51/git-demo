package com.anil.gitdemo.network.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import static com.anil.gitdemo.network.model.Status.ERROR;
import static com.anil.gitdemo.network.model.Status.LOADING;
import static com.anil.gitdemo.network.model.Status.SUCCESS;

public class CommentResponse {

    public final Status status;


    public  List<Comments> data;

    @Nullable
    public final Throwable error;

    private CommentResponse(Status status, @Nullable List<Comments> data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static CommentResponse loading() {
        return new CommentResponse(LOADING, null, null);
    }

    public static CommentResponse success(@NonNull List<Comments> data) {
        return new CommentResponse(SUCCESS, data, null);
    }

    public static CommentResponse error(@NonNull Throwable error) {
        return new CommentResponse(ERROR, null, error);
    }
}