package com.anil.gitdemo.network.model;
/**
 * Possible status types of a response provided to the UI
 */
public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}
