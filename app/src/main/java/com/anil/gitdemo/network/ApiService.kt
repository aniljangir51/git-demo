package com.anil.gitdemo.network

import com.anil.gitdemo.network.model.Comments
import com.anil.gitdemo.network.model.Issues
import com.anil.gitdemo.network.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("issues")
    fun getIssues(): Single<List<Issues>>

    @GET("issues/{id}/comments")
    fun getComments(@Path("id") id:Int): Single<List<Comments>>

}