package com.anil.gitdemo.viewmodels;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.anil.gitdemo.application.MyUIApplication;
import com.anil.gitdemo.database.DatabaseClient;
import com.anil.gitdemo.network.ApiClient;
import com.anil.gitdemo.network.ApiService;
import com.anil.gitdemo.network.model.DateModel;
import com.anil.gitdemo.network.model.Issues;
import com.anil.gitdemo.network.model.Response;
import com.anil.gitdemo.utils.DateConverter;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();

    public MutableLiveData<Response> responce= new MutableLiveData<>();

    public void getIssues(){

        Log.e("date",""+DateConverter.fromDate(new Date()));
        List<DateModel> list=MyUIApplication.getInstance().getAppDatabase()
                .dateDataDeo()
                //.getAllData();
                .getDatabyDate(DateConverter.fromDate(new Date()));
        Log.e("list",""+list);

        if (list.size()>0)
            getLocalIssues();
        else
            disposables.add(ApiClient.getClient().create(ApiService.class)
                    .getIssues()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> responce.setValue(Response.loading()))
                    .subscribeWith(new DisposableSingleObserver<List<Issues>>(){

                        @Override
                        public void onSuccess(List<Issues> list) {
                            responce.setValue(Response.success(list));

                            saveInLocalDB(list);
                        }

                        @Override
                        public void onError(Throwable e) {
                            responce.setValue(Response.error(e));
                        }
                    }));
    }

    private void saveInLocalDB(List<Issues> list) {
        DeleteDate();
        saveDate();

        Issues[] array=list.toArray(new Issues[list.size()]);
        List mlist=MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .insert(array);
        Log.e("data inserted local Db",""+mlist);

       /* MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .insert(list.toArray(new Issues[list.size()]))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Long>>() {
                    @Override
                    public void onSuccess(List<Long> longs) {
                        Log.e("data inserted local Db",""+longs);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.e("Error",""+e.getMessage());
                    }
                });*/
    }

    void getLocalIssues(){
        List missues=MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .getIssues();
        Log.e("issues",""+missues);
        responce.setValue(Response.success(missues));
        /*MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .getIssues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Issues>>() {
                    @Override
                    public void onSuccess(List<Issues> list) {
                        responce.setValue(Response.success(list));
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        responce.setValue(Response.error(e));
                    }
                });*/
    }

    void clearAllIssues(){
        int counts= MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .deleteAll();
        Log.e("delete Issues",""+counts);
       /* MyUIApplication.getInstance().getAppDatabase()
                .issueDataDeo()
                .deleteAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        Log.e("delete Issues",""+integer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.e("delete Issues Error",""+e.getMessage());
                    }
                });*/
    }

    private void saveDate() {

        List counts=MyUIApplication.getInstance().getAppDatabase()
                .dateDataDeo()
                .insert(new DateModel(new Date()));
        Log.e("date Saved",""+counts);


    }
    private void DeleteDate() {

        clearAllIssues();

        int deleted= MyUIApplication.getInstance().getAppDatabase()
                .dateDataDeo()
                .deleteAll();
        Log.e("delete dates",""+deleted);

    }
    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }
}
