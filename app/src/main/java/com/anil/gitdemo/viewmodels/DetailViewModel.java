package com.anil.gitdemo.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.anil.gitdemo.network.ApiClient;
import com.anil.gitdemo.network.ApiService;
import com.anil.gitdemo.network.model.CommentResponse;
import com.anil.gitdemo.network.model.Comments;
import com.anil.gitdemo.network.model.Issues;
import com.anil.gitdemo.network.model.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DetailViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();

    public MutableLiveData<CommentResponse> responce= new MutableLiveData<>();

    public void getComments(int id){

        disposables.add(ApiClient.getClient().create(ApiService.class)
                .getComments(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> responce.setValue(CommentResponse.loading()))
                .subscribeWith(new DisposableSingleObserver<List<Comments>>(){

                    @Override
                    public void onSuccess(List<Comments> list) {
                        responce.setValue(CommentResponse.success(list));
                    }

                    @Override
                    public void onError(Throwable e) {
                        responce.setValue(CommentResponse.error(e));
                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }
}
