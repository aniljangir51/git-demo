package com.anil.gitdemo

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anil.gitdemo.adapters.CommentsAdpter
import com.anil.gitdemo.adapters.IssuesAdpter
import com.anil.gitdemo.databinding.ActivityDetailBinding
import com.anil.gitdemo.databinding.ActivityMainBinding
import com.anil.gitdemo.network.model.*
import com.anil.gitdemo.viewmodels.DetailViewModel
import com.anil.gitdemo.viewmodels.MainViewModel

class DetailActivity: AppCompatActivity() {

    lateinit var mainModel:Issues
    var mList=ArrayList<Comments>()
    var adapter = CommentsAdpter(this,mList,null)
    lateinit var binding: ActivityDetailBinding
    lateinit var mModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.title= "Anil"
        initAdapter()

        mainModel = intent.getParcelableExtra("model")
        binding.model= mainModel

        mModel= ViewModelProviders.of(this).get(DetailViewModel::class.java)

        mModel.responce.observe(this, Observer {
            processResponce(it)
        })
        mModel.getComments(mainModel.number)

    }

    private fun processResponce(it: CommentResponse?) {
        when(it?.status){
            Status.SUCCESS ->{
                binding.isLoading=false
                mList.clear()
                mList.addAll(it.data)
                adapter.notifyDataSetChanged()
            }

            Status.ERROR ->{
                binding.isLoading=false
                binding.isNodata=true
            }
            Status.LOADING ->{
                binding.isLoading=true
            }
        }

    }

    private fun initAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        binding.recyclerView.itemAnimator = DefaultItemAnimator()
        //  binding.recyclerView.addItemDecoration(MyDividerItemDecoration(this,RecyclerView.VERTICAL,16))
        binding.recyclerView.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }

}